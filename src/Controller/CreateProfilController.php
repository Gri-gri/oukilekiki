<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class CreateProfilController extends AbstractController
{
    /**
     * @Route("/create/profil", name="create_profil")
     */
    public function index()
    {
        return $this->render('create_profil/index.html.twig', [
            'controller_name' => 'CreateProfilController',
        ]);
    }
}
