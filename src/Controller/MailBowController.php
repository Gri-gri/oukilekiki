<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class MailBowController extends AbstractController
{
    /**
     * @Route("/mail/bow", name="mail_bow")
     */
    public function index()
    {
        return $this->render('mail_bow/index.html.twig', [
            'controller_name' => 'MailBowController',
        ]);
    }
}
