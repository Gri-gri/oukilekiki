<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;

class ModifyProfilController extends AbstractController
{
    /**
     * @Route("/modify/profil", name="modify_profil")
     */
    public function index()
    {
        return $this->render('modify_profil/index.html.twig', [
            'controller_name' => 'ModifyProfilController',
        ]);
    }
}
